const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Company = require('../models/company');
const Domain = require('../models/domain');
const crypto = require('crypto');
const { Resolver } = require('dns').promises;
const resolver = new Resolver();

//@desc   create domain for company
//@route  POST /domain/create
//@access PRIVATE : 'VERIFIED'
exports.createDomain = asyncHandler(async (req, res, next) => {
    const companyId = req.user.comId;
    const { domain } = req.body;

    if (!domain) return next(new ErrorResponse('Please provide domain!', 400));

    const company = await Company.findOne({ _id: companyId }).select('name domains');
    //TODO find better way to get time
    const verification_key =
        'hac-ur-zone=' +
        crypto
            .createHmac('sha256', process.env.JWT_SECRET_KEY)
            .update(company.name + domain + new Date().toLocaleTimeString())
            .digest('hex');

    const domainModel = new Domain({
        domain,
        verification_key,
    });

    const newDomain = await domainModel.save();

    company.domains.push(newDomain);

    await company.save();

    return res.status(201).json({ success: true, domainModel });
});

//@desc   verify domain
//@route  POST /domain/verify/:id
//@access PRIVATE : 'VERIFIED'
// TODO test this controller
exports.verifyDomain = async (req, res, next) => {
    const companyId = req.user.comId;
    const domainId = req.params.id;

    if (!domainId) return next(new ErrorResponse('Please provide domain', 400));

    const company = await Company.findOne({ _id: companyId })
        .select('domains')
        .populate({ path: 'domains', match: { _id: domainId } });

    if (company.domains.length === 0) return next(new ErrorResponse('Domain is not yours', 403));

    addresses = await resolver.resolveTxt(company.domains[0].domain);

    const result = addresses.find(address => address === company.domains[0].verification_key);

    if (result === undefined) return next(new ErrorResponse('Domain can not be verified!'), 403);

    await Domain.findOneAndUpdate(
        { _id: domainId },
        {
            $set: {
                status: 'VERIFIED',
            },
        },
        { new: true, runValidators: true }
    );

    return res.status(200).json({ success: true, message: 'Your domain is verified!' });
};

//@desc   Delete domain
//@route  DELETE /domain/:id
//@access PRIVATE : 'VERIFIED'
exports.deleteDomain = asyncHandler(async (req, res, next) => {
    const companyId = req.user.comId;
    const domainId = req.params.id;

    const company = await Company.findById(companyId)
        .select('domains')
        .populate({ path: 'domains', match: { _id: domainId } });

    if (company.domains.length === 0) return next(new ErrorResponse('Domain does not exist!', 404));

    company.domains.remove(domainId);
    await company.save();

    await Domain.findByIdAndDelete(domainId);

    return res.status(200).json({
        success: true,
        message: `Domain Deleted Successfully!`,
    });
});