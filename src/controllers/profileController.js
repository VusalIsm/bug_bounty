const express = require('express');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Hacker = require('../models/hacker');
const Responsible = require('../models/responsible');
const Company = require('../models/company');
const Report = require('../models/report');
const { select } = require('async');
const Currency = require('../models/currency');
const Bounty = require('../models/bounty');
const bounty = require('../models/bounty');
const { report } = require('../routes/hacker');

//@desc   hacker's info page / profile overview
//@route  GET /profile/hacker/:id
//@access PUBLIC
exports.getHackerInfo = asyncHandler(async (req, res, next) => {
  const id = req.params.id;

  const hacker = await Hacker.findById(id)
    .select('reports first_name last_name username avatar bio linkedin facebook instagram status cv reputation is_private')
    .populate({
      path: 'reports',
      select: 'name',
      match: { is_private: false, status: 'ACCEPTED' },
    });

  if (!hacker) {
    return next(new ErrorResponse('Requested URL Not Found.', 404));
  }

  if (hacker.is_private) {
    return res.status(403).json({
      success: true,
      is_private: hacker.is_private,
      message: `This profile is private!`,
    });
  }
  return res.status(200).json({
    success: true,
    message: `Welcome to Hacker's Info Page!`,
    hacker,
  });
});

//@desc   hacker's dashboard view
//@route  GET /profile/hacker/dashboard
//@access PRIVATE : 'HACKER','PRO'
exports.getHackerDB = asyncHandler(async (req, res, next) => {
  const hacker = await Hacker.findOne({ _id: req.user.hackerId })
    .select(
      'reports first_name last_name username bio avatar home_address email voen linkedin facebook instagram status cv reputation is_private'
    )
    .populate({
      path: 'reports',
      select: 'name is_private',
    });

  if (!hacker) {
    return next(new ErrorResponse('Something went wrong.', 404));
  }

  return res.status(200).json({
    success: true,
    message: `Welcome to Your Dashboard!`,
    hacker,
    nbReports: hacker.reports.length,
  });
});

//@desc   hacker's dashboard edit
//@route  PUT /profile/hacker/dashboard
//@access PRIVATE : 'HACKER','PRO'
exports.updateHackerDB = asyncHandler(async (req, res, next) => {
  const update = ({
    first_name,
    last_name,
    username,
    oldPassword,
    newPassword,
    newPasswordConfirmation,
    bio,
    home_address,
    voen,
    linkedin,
    facebook,
    instagram,
    is_private,
  } = req.body);

  const hacker = await Hacker.findOne({ _id: req.user.hackerId });

  if (!hacker) {
    return next(new ErrorResponse('Requested URL Not Found.', 404));
  }

  if (!update.oldPassword || update.oldPassword === '') {
    return next(new ErrorResponse('You must enter your password to update your profile!', 403));
  }

  const isMatch = hacker.isMatchedPassword(oldPassword);

  if (!isMatch) {
    return next(new ErrorResponse('Password is incorrect!', 403));
  }

  const updatedHacker = await Hacker.findOneAndUpdate({ _id: req.user.hackerId }, update, { new: true, runValidators: true }).select(
    'reports first_name last_name username bio avatar home_address email voen linkedin facebook instagram status cv reputation is_private'
  );

  return res.status(200).json({
    success: true,
    message: `Profile Updated Successfully!`,
    updatedHacker,
  });
});

//@desc   save company's public key
//@route  POST /profile/key
//@access PRIVATE
exports.saveCompanyPublicKey = asyncHandler(async (req, res, next) => {
  const companyId = req.user.comId;
  const { public_key } = req.body;

  const company = await Company.findOneAndUpdate(
    { _id: companyId },
    {
      $set: {
        public_key: public_key,
      },
    },
    { new: true, runValidators: true }
  );

  return res.status(200).json({ success: true, message: 'Public key saved successfully!' });
});

//@desc   get company's public key
//@route  POST /profile/company/key/:comId
//@access PRIVATE
exports.getCompanyPublicKey = asyncHandler(async (req, res, next) => {
  const companyId = req.params.comId;

  const company = await Company.findOne({ _id: companyId }).select('public_key');

  return res.status(200).json({ success: true, public_key: company.public_key });
});

//@desc   company's info page / profile overview
//@route  GET /profile/company/:id
//@access PUBLIC
exports.getCompanyInfo = asyncHandler(async (req, res, next) => {
  const id = req.params.id;

  const company = await Company.findOne({ _id: id })
    .select('domains bounties name description logo address contact_number email linkedin facebook instagram')
    .populate({
      path: 'domains',
      select: 'domain',
      match: { status: 'VERIFIED' },
    })
    .populate({
      path: 'bounties',
      select: 'name',
      match: { is_private: false },
    });

  if (!company) {
    return next(new ErrorResponse('Requested URL Not Found.', 404));
  }

  return res.status(200).json({
    success: true,
    message: `Welcome to Company's Info Page!`,
    company,
  });
});

//@desc   company's dashboard view
//@route  GET /profile/company/dashboard
//@access PRIVATE : 'VERIFIED'
exports.getCompanyDB = asyncHandler(async (req, res, next) => {
  const company = await Company.findOne({ _id: req.user.comId })
    .select('domains bounties name description logo address contact_number email status linkedin facebook instagram')
    .populate({
      path: 'domains',
    });

  if (!company) {
    return next(new ErrorResponse('Something went wrong.', 404));
  }

  return res.status(200).json({
    success: true,
    message: `Welcome to ${company.name}'s Dashboard!`,
    company,
  });
});

//@desc   responsible's dashboard view
//@route  GET /profile/responsible/dashboard
//@access PRIVATE : 'VERIFIED'
exports.getResponsibleDB = asyncHandler(async (req, res, next) => {
  const responsible = await Responsible.findOne({ _id: req.user.resId }).select('first_name last_name email position');

  if (!responsible) {
    return next(new ErrorResponse('Something went wrong.', 404));
  }

  return res.status(200).json({
    success: true,
    message: `Welcome to Your Dashboard, ${responsible.first_name} ${responsible.last_name}!`,
    responsible,
  });
});

//@desc   company's dashboard edit
//@route  PUT /profile/company/dashboard
//@access PRIVATE : 'VERIFIED'
exports.updateCompanyDB = asyncHandler(async (req, res, next) => {
  const update = ({ bounties, name, address, contact_number, email, linkedin, facebook, instagram } = req.body);

  const company = await Company.findOneAndUpdate({ _id: req.user.comId }, update, { new: true, runValidators: true });

  if (!company) {
    return next(new ErrorResponse('Requested URL Not Found.', 404));
  }

  return res.status(200).json({
    success: true,
    message: `Profile Updated Successfully!`,
    company,
  });
});

//@desc   responsible's dashboard edit
//@route  PUT /profile/hacker/dashboard
//@access PRIVATE : 'HACKER','PRO'
exports.updateResponsibleDB = asyncHandler(async (req, res, next) => {
  const update = ({ first_name, last_name, position, email } = req.body);

  const responsible = await Responsible.findOneAndUpdate({ _id: req.user.resId }, update, { new: true, runValidators: true });

  if (!responsible) {
    return next(new ErrorResponse('Requested URL Not Found.', 404));
  }

  return res.status(200).json({
    success: true,
    message: `Profile Updated Successfully!`,
    responsible,
  });
});

//@desc   Get All reports of the Company
//@route  GET /profile/reports
//@access PRIVATE : 'VERIFIED'
exports.getReportsForCompany = asyncHandler(async (req, res, next) => {
  const reports = await Report.find().populate({ path: 'bounty', select: 'company name' });
  const result = reports.filter(report => report.bounty.company == req.user.comId);
  return res.status(200).json({ success: true, reports: result });
});

//@desc   Get reports of a special bounty of the Company
//@route  GET /profile/reports/:id
//@access PRIVATE : 'VERIFIED'
exports.getReportsOfBounty = asyncHandler(async (req, res, next) => {
  const { bounties } = await Company.findById(req.user.comId)
    .select('bounties')
    .populate({
      path: 'bounties',
      select: { reports: 1 },
      match: { _id: req.params.id },
      populate: {
        path: 'reports',
        select: { name: 1, author: 1, status: 1 },
      },
    });
  if (bounties.length) {
    return res.status(200).json({ success: true, reports: bounties[0].reports });
  }
  res.status(404).json({ success: false, msg: 'Not found' });
});

//@desc   Get verified domains of the company
//@route  GET /profile/domains
//@access PRIVATE : 'VERIFIED'
exports.getDomainsForCompany = asyncHandler(async (req, res, next) => {
  const { domains } = await Company.findById(req.user.comId)
    .select('domains')
    .populate({
      path: 'domains',
      select: 'domain',
      match: { status: 'VERIFIED' },
    });
  res.status(200).json({ success: true, domains });
});

//@desc   Get All Currencies in DB
//@route  GET /profile/currencies
//@access PRIVATE : 'HACKER', 'PRO', 'VERIFIED'
exports.getAllCurrencies = asyncHandler(async (req, res, next) => {
  const currencies = await Currency.find({});

  return res.status(200).json({
    success: true,
    message: `Returning every currency in the DB `,
    currencies,
  });
});
