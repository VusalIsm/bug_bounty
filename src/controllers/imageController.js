const express = require('express');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Hacker = require('../models/hacker');
const Company = require('../models/company');
const path = require('path');
const { PATH } = require('../constants/paths');

//@desc   Upload (set) new avatar for Hackers
//@route  POST /image/avatar
//@access PRIVATE : 'HACKER','PRO'
exports.setHackerAvatar = asyncHandler(async (req, res, next) => {
    const hackerId = req.user.hackerId;

    const avatar = path.basename(req.file.path);

    const hacker = await Hacker.findOneAndUpdate({ _id: hackerId }, { avatar }, { new: true, runValidators: true }).select('avatar');
    if (!hacker) { 
        return next(new ErrorResponse('Something went wrong.', 500));
    }
    
    return res.status(200).json({
        success: true,
        message: `Avatar Uploaded Successfully`,
        hacker 
    });
});

//@desc   Upload (set) new logo for Companies
//@route  POST /image/logo
//@access PRIVATE : 'VERIFIED'
exports.setCompanyLogo = asyncHandler(async (req, res, next) => {
    const comId = req.user.comId;

    const logo = path.basename(req.file.path);

    const company = await Company.findOneAndUpdate({ _id: comId }, { logo }, { new: true, runValidators: true }).select('logo');
    if (!logo) { 
        return next(new ErrorResponse('Something went wrong.', 500));
    }
    
    return res.status(200).json({
        success: true,
        message: `Logo Uploaded Successfully`,
        company 
    });
});

//@desc   Get current avatar for Hackers
//@route  GET /image/avatar/:path
//@access PRIVATE 'AUTHENTICATED'
exports.getHackerAvatar = asyncHandler(async (req, res, next) => {
    const avatar = req.params.path;

    const hacker = await Hacker.findOne({ avatar }).select('is_private avatar');
    if (!hacker) {
        return next(new ErrorResponse('Something went wrong.', 404));
    }

    if (hacker.is_private) {
        return res.status(403).json({
            success: true,
            is_private: hacker.is_private,
            message: `This profile is private and can only be seen by other hackers!`,
        });
    }

    return res.sendFile(`${PATH.AVATAR}/${avatar}`);
});

//@desc   Get current logo for Companies
//@route  GET /image/logo/:path
//@access PRIVATE 'AUTHENTICATED'
exports.getCompanyLogo = asyncHandler(async (req, res, next) => {
    const logo = req.params.path;

    const company = await Company.findOne({ logo }).select('logo');
    if (!company) {
        return next(new ErrorResponse('Something went wrong.', 404));
    }

    return res.sendFile(`${PATH.LOGO}/${logo}`);
});