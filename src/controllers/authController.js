const express = require('express');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Hacker = require('../models/hacker');
const Company = require('../models/company');
const Responsible = require('../models/responsible');
const jwt = require('jsonwebtoken');
const { sendMail } = require('../utils/mailHandler');

//@desc   registration for hackers
//@route  POST /auth/hacker/register
//@access PUBLIC
exports.createHacker = asyncHandler(async (req, res, next) => {
  const { first_name, last_name, username, password, passwordConfirmation, home_address, email, voen } = req.body;

  if (password != passwordConfirmation) {
    next(new ErrorResponse('Passwords are not matched!', 400));
  }

  const newHacker = new Hacker({
    first_name,
    last_name,
    username,
    password,
    home_address,
    email,
    voen,
  });

  await newHacker.save();

  const token = newHacker.getSignedJWTToken();

  await sendMail({ mailTo: email, mailType: 'HACKER_REGISTRATION', options: { username, id: newHacker._id, token } });

  return res.status(201).json({ success: true, message: 'You successfully created a user!' });
});

//@desc   registration for companies
//@route  POST /auth/company/register
//@access PUBLIC
exports.createCompany = asyncHandler(async (req, res, next) => {
  const { first_name, last_name, emailRes, password, passwordConfirmation, position } = req.body.responsible;
  const { name, address, contact_number, emailCom } = req.body.company;

  if (password != passwordConfirmation) {
    return next(new ErrorResponse('Passwords are not matched!', 400));
  }

  const newCompany = new Company({
    name,
    address,
    contact_number,
    email: emailCom,
  });

  await newCompany.save();

  const responsible = new Responsible({
    company: newCompany._id,
    first_name,
    last_name,
    email: emailRes,
    password,
    position,
  });

  try {
    await responsible.save();
  } catch (error) {
    console.log(error);
    await Company.findByIdAndDelete({ _id: newCompany._id });
    return next(error);
  }

  const token = responsible.getSignedJWTToken();

  await sendMail({
    mailTo: emailRes,
    mailType: 'COMPANY_REGISTRATION',
    options: { fullname: `${first_name} ${last_name}`, company_name: newCompany.name, token },
  });

  return res.status(201).json({ success: true, message: 'Company created!' });
});

//@desc   Activate account
//@route  GET /auth/verify/:token
//@access PUBLIC
exports.activateAccount = asyncHandler(async (req, res, next) => {
  const token = req.params.token;
  const decoded = await jwt.verify(token, process.env.JWT_SECRET_KEY);
  if (decoded.resId && decoded.comId) {
    await Company.updateOne({ _id: decoded.comId }, { status: 'VERIFIED' });
  } else if (decoded.hackerId) {
    await Hacker.updateOne({ _id: decoded.hackerId }, { status: 'HACKER' });
  } else {
    return next(new Response());
  }

  return res.redirect(process.env.FRONT_URL);
});

//@desc   login for hackers
//@route  POST /auth/hacker/login
//@access PUBLIC
exports.loginHacker = asyncHandler(async (req, res, next) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return next(new ErrorResponse('Please provide email and password!', 400));
  }

  const hacker = await Hacker.findOne({ email }).select('username email password status');

  if (!hacker) {
    return next(new ErrorResponse('Provided email is not correct!', 403));
  }

  if (hacker.status === 'PENDING') {
    return next(new ErrorResponse('Please verify your account!', 403));
  }

  const isMatch = hacker.isMatchedPassword(password);

  if (!isMatch) {
    return next(new ErrorResponse('Password is incorrect!', 403));
  }

  const token = hacker.getSignedJWTToken();
  sendTokenInCookie(token, 200, res);
});

//@desc   login for company
//@route  POST /auth/company/login
//@access PUBLIC
exports.loginCompany = asyncHandler(async (req, res, next) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return next(new ErrorResponse('Please provide email and password!', 400));
  }

  const responsible = await Responsible.findOne({ email }).populate('company').select('email password company public_key');

  if (!responsible) {
    return next(new ErrorResponse('Provided email is not correct!', 403));
  }

  if (responsible.company.status === 'PENDING') {
    return next(new ErrorResponse('Please verify your account!', 401));
  }

  const isMatch = responsible.isMatchedPassword(password);
  if (!isMatch) {
    return next(new ErrorResponse('Password is incorrect!', 403));
  }

  const token = responsible.getSignedJWTToken();
  sendTokenInCookie(token, 200, res, { public_key: responsible.company.public_key });
});

//@desc   Email resend route
//@route  POST /auth/resend
//@access PUBLIC
exports.postEmailResend = asyncHandler(async (req, res, next) => {
  try {
    const { email, mailType } = req.body;
    let user;
    let options;

    if (mailType == 'HACKER_REGISTRATION') {
      user = await Hacker.findOne({ email, status: 'PENDING' }).select('username');
      options = { username: user.username, id: user._id };
    } else if (mailType == 'COMPANY_REGISTRATION') {
      user = await Responsible.findOne({ email })
        .select('company first_name last_name')
        .populate({
          path: 'company',
          select: { name: 1 },
          match: { status: 'PENDING' },
        });
      options = { fullname: `${user.first_name} ${user.last_name}`, company_name: user.company.name };
    }

    options['token'] = user.getSignedJWTToken();
    sendMail({
      mailTo: email,
      mailType,
      options,
    });
  } catch (err) {}
  return res.status(200).json({ success: true, message: 'Email sent!' });
});

//@desc   Sign out route
//@route  POST /auth/signout
//@access PUBLIC
exports.postSignOut = asyncHandler(async (req, res, next) => {
  res.clearCookie('token').status(200).json({ success: true, message: 'Signed out successfully!' });
});

// Sent token in cookie
const sendTokenInCookie = (token, statusCode, res, optional) => {
  const options = {
    expires: new Date(Date.now() + 3600000),
    httpOnly: true,
  };

  const response = {
    success: true,
    message: 'Logged in!',
    optional,
  };

  return res.status(statusCode).cookie('token', token, options).json(response);
};
