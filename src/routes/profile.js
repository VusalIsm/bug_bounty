const express = require('express');
const router = express.Router();
const { PERMISSIONS } = require('../constants/permissions');
const { protect } = require('../middleware/protect');
const {
  getHackerInfo,
  getHackerDB,
  updateHackerDB,
  getCompanyInfo,
  getCompanyDB,
  updateCompanyDB,
  getResponsibleDB,
  updateResponsibleDB,
} = require('../controllers/profileController');
const {
  getReportsForCompany,
  getReportsOfBounty,
  getDomainsForCompany,
  getAllCurrencies,
  saveCompanyPublicKey,
  getCompanyPublicKey,
} = require('../controllers/profileController');
const { createBounty, getBounty, updateBounty, deleteBounty } = require('../controllers/bountyController');

// Dashboard Routes for Hackers
router.get('/hacker/dashboard', protect(PERMISSIONS.ONLY_HACKERS), getHackerDB);
router.put('/hacker/dashboard', protect(PERMISSIONS.ONLY_HACKERS), updateHackerDB);

// Dashboard Routes for Companies and Responsibles
router.get('/company/dashboard', protect(PERMISSIONS.ONLY_COMPANIES), getCompanyDB);
router.get('/responsible/dashboard', protect(PERMISSIONS.ONLY_COMPANIES), getResponsibleDB);
router.put('/company/dashboard', protect(PERMISSIONS.ONLY_COMPANIES), updateCompanyDB);
router.put('/responsible/dashboard', protect(PERMISSIONS.ONLY_COMPANIES), updateResponsibleDB);

// Bounty routes for Companies - create, get, delete
router.post('/company/bounty/create', protect(PERMISSIONS.ONLY_COMPANIES), createBounty);
router.get('/company/bounty/:id', protect(PERMISSIONS.ONLY_COMPANIES), getBounty);
router.put('/company/bounty/:id', protect(PERMISSIONS.ONLY_COMPANIES), updateBounty);
router.delete('/company/bounty/:id', protect(PERMISSIONS.ONLY_COMPANIES), deleteBounty);

// Endpoint to retrieve all verified domains for companies
router.get('/company/domains', protect(PERMISSIONS.ONLY_COMPANIES), getDomainsForCompany);

// Report routes for companies
router.get('/reports', protect(PERMISSIONS.ONLY_COMPANIES), getReportsForCompany);
router.get('/reports/:id', protect(PERMISSIONS.ONLY_COMPANIES), getReportsOfBounty);
router.post('/key', protect(PERMISSIONS.ONLY_COMPANIES), saveCompanyPublicKey);
router.get('/company/key/:comId', protect(PERMISSIONS.ONLY_HACKERS), getCompanyPublicKey);

// Overview of the profile (GUESS ACCESS)
router.get('/hacker/:id', getHackerInfo);
router.get('/company/:id', getCompanyInfo);

// Get all currencies in the database
router.get('/currencies', protect(PERMISSIONS.ONLY_AUTHENTICATED), getAllCurrencies);

module.exports = router;
