const express = require('express');
const router = express.Router();
const Hacker = require('../models/hacker');
const Company = require('../models/company');
const Bounty = require('../models/bounty');
const Currency = require('../models/currency');
const { paginate } = require('../middleware/pagination');
const { getBountyForHackers, getLeaderboard, getHackerReports, submitReport } = require('../controllers/hackerController');
const { PERMISSIONS } = require('../constants/permissions');
const { protect } = require('../middleware/protect');
const asyncHandler = require('../middleware/async');

router.get(
  '/leaderboard',
  paginate([Hacker, 'hacker'], { status: ['HACKER', 'PRO'] }, 'first_name last_name username bio avatar reputation is_private status', {
    reputation: '-1',
  }),
  getLeaderboard
);

router.get('/bounty/:id', protect(PERMISSIONS.ONLY_HACKERS), getBountyForHackers);

router.post('/report/create', protect(PERMISSIONS.ONLY_HACKERS), submitReport);

//@desc   List of companies
//@route  GET /hacker/companies
//@access PRIVATE : 'HACKER','PRO'
router.get(
  '/companies',
  protect(PERMISSIONS.ONLY_HACKERS),
  paginate([Company, 'company'], { status: ['VERIFIED'], bounties: { $ne: [] } }, 'name logo description', { date: 1 }),
  asyncHandler(async (req, res, next) => res.status(200).json({ success: true, results }))
);

//@desc   List of bounties
//@route  GET /hacker/bounties
//@access PRIVATE : 'HACKER','PRO'
router.get(
  '/bounties',
  protect(PERMISSIONS.ONLY_HACKERS),
  paginate(
    [Bounty, 'bounty'],
    [{ HACKER: { is_private: false }, PRO: {} }],
    'name company domains policy created_at low_price medium_price high_price currency',
    { date: 1 },
    { path: 'currency domains company', select: 'short_name -_id domain name logo' }
  ),
  asyncHandler(async (req, res, next) => res.status(200).json({ success: true, results }))
);

//@desc  Get all submitted reports
//@route  GET /hacker/reports
//@access PRIVATE : 'HACKER','PRO'
router.get('/reports', protect(PERMISSIONS.ONLY_HACKERS), getHackerReports);

module.exports = router;
