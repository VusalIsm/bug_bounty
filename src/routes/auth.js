const express = require('express');
const router = express.Router();
const {
  createHacker,
  loginHacker,
  createCompany,
  loginCompany,
  activateAccount,
  postEmailResend,
  postSignOut
} = require('../controllers/authController');
const {
    forgetPasswordHacker,
    resetPasswordHacker,
    forgetPasswordCompany,
    resetPasswordCompany,
} = require('../controllers/resetController');

router.post('/hacker/register', createHacker);
router.post('/hacker/login', loginHacker);
router.post('/company/register', createCompany);
router.post('/company/login', loginCompany);
router.get('/verify/:token', activateAccount);
router.post('/resend', postEmailResend);
router.post('/signout', postSignOut);

// Reset password routes
router.post('/hacker/forget', forgetPasswordHacker);
router.post('/hacker/reset/:token', resetPasswordHacker);
router.post('/company/forget', forgetPasswordCompany);
router.post('/company/reset/:token', resetPasswordCompany);

module.exports = router;
