const express = require('express');
const router = express.Router();
const { protect } = require('../middleware/protect');
const { PERMISSIONS } = require('../constants/permissions');
const { createDomain, verifyDomain, deleteDomain } = require('../controllers/domainController');

router.post('/create', protect(PERMISSIONS.ONLY_COMPANIES), createDomain);
router.post('/verify/:id', protect(PERMISSIONS.ONLY_COMPANIES), verifyDomain);
router.delete('/:id', protect(PERMISSIONS.ONLY_COMPANIES), deleteDomain);

module.exports = router;
