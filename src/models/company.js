const mongoose = require('mongoose');
const { PATH } = require('../constants/paths');

const companySchema = mongoose.Schema({
  domains: [
    {
      type: mongoose.Types.ObjectId,
      ref: 'Domain',
    },
  ],
  bounties: [
    {
      type: mongoose.Types.ObjectId,
      ref: 'Bounty',
    },
  ],
  public_key: {
    type: String,
    default: '',
  },
  name: {
    type: String,
    validate: {
      validator: function (name) {
        return /^\w[\w.\-#&\s]*$/.test(name);
      },
      message: props => `${props.value} is not a valid company / business name!`,
    },
    required: [true, 'Company name required!'],
    unique: [true, 'Company name must be unique!'],
  },
  description: {
    type: String,
    trim: true,
    default: '',
  },
  logo: {
    type: String,
    default: 'default-logo.png',
  },
  address: {
    type: String,
    required: [true, 'Company address is required!'],
    max: 100,
  },
  contact_number: {
    type: Number,
    min: [11, 'Contact number must contain at least 11 digit'],
    required: [true, 'Contact number required!'],
    unique: [true, 'Contact number must be unique!'],
  },
  email: {
    type: String,
    validate: {
      validator: function (email) {
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
          email
        );
      },
      message: props => `${props.value} is not a valid e-mail address!`,
      // bu regexde email a@salam.b sheklinde ola bilmez noqteden sonra en azi 2 herf gelmelidir cunki 1 herfli domain sonlugu yoxdur
    },
    required: [true, 'E-mail is required!'],
    trim: true,
    // unique: [true, 'E-mail address is already registered!'],
  },
  status: {
    type: String,
    enum: ['PENDING', 'VERIFIED'],
    default: 'PENDING',
  },
  linkedin: {
    type: String,
    // unique: [true, 'Account in the given URL is already in use!'],
    validate: {
      validator: function (url) {
        if (/^$/.test(url)) return true;
        return /^((https?:\/\/)?((www|\w\w)\.)?linkedin\.com\/)((([\w]{2,3})?)|([^\/]+\/(([\w|\d-&#?=])+\/?){1,}))$/.test(url);
      },
      message: props => `${props.value} is not a valid Linkedin URL!`,
      // LinkedIn ucun xususi URL regexi
    },
    sparse: true,
    default: '',
  },
  facebook: {
    type: String,
    // unique: [true, 'Account in the given URL is already in use!'],
    validate: {
      validator: function (url) {
        if (/^$/.test(url)) return true;
        return /^(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?$/.test(
          url
        );
      },
      message: props => `${props.value} is not a valid Facebook profile / page!`,
      // Facebook ucun xususi URL regexi
    },
    sparse: true,
    default: '',
  },
  instagram: {
    type: String,
    // unique: [true, 'Account in the given URL is already in use!'],
    validate: {
      validator: function (url) {
        if (/^$/.test(url)) return true;
        return /^\s*(http\:\/\/)?instagram\.com\/[a-z\d-_]{1,255}\s*$/i.test(url);
      },
      message: props => `${props.value} is not a valid Instagram URL!`,
      // Instagram ucun xususi URL regexi
    },
    sparse: true,
    default: '',
  },
});

module.exports = mongoose.model('Company', companySchema);
